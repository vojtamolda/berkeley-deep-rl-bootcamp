#!/usr/bin/env python3

import pickle
import argparse
from importlib import import_module

import wandb
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import ignite.contrib.handlers

import gym
import roboschool

from run_expert import execute_rollout


clone_map = {
    'RoboschoolAnt-v1': import_module('clones.RoboschoolAnt-v1').RoboschoolAnt,
    'RoboschoolHalfCheetah-v1': import_module('clones.RoboschoolHalfCheetah-v1').RoboschoolHalfCheetah,
    'RoboschoolHopper-v1': import_module('clones.RoboschoolHopper-v1').RoboschoolHopper,
    'RoboschoolHumanoid-v1': import_module('clones.RoboschoolHumanoid-v1').RoboschoolHumanoid,
    'RoboschoolReacher-v1': import_module('clones.RoboschoolReacher-v1').RoboschoolReacher,
    'RoboschoolWalker2d-v1': import_module('clones.RoboschoolWalker2d-v1').RoboschoolWalker2d,
}


class ExpertPolicyRollouts(data.Dataset):
    def __init__(self, rollout_dataset_file):
        super().__init__()
        dataset = pickle.load(open(rollout_dataset_file, 'rb'))
        self.actions = torch.from_numpy(dataset['actions'])
        self.observations = torch.from_numpy(dataset['observations'])

    def __getitem__(self, index):
        action = self.actions[index]
        observation = self.observations[index]
        return observation, action

    def __len__(self):
        return self.observations.shape[0]


class EnvEpisodeReward(ignite.metrics.Metric):
        def __init__(self, env, policy, num_rollouts=3):
            super().__init__()
            self.env, self.policy, self.num_rollouts = env, policy, num_rollouts

        def reset(self):
            pass

        def update(self, output):
            pass

        def compute(self):
            _, _, rewards = execute_rollout(self.env, self.policy, self.num_rollouts)
            return rewards.mean()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Train a policy clone from an dataset of expert rollouts.')
    parser.add_argument('env_id', type=str, metavar='roboschool-env', choices=clone_map.keys(),
                        help='roboschool environment id')
    parser.add_argument('dataset', type=str, default=None, metavar='dataset',
                        help='pickle file with the expert policy rollout dataset')
    parser.add_argument('--num_epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 50)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                        help='input batch size for training (default: 128)')
    parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
                        help='optimizer learning rate (default: 0.001)')
    args = parser.parse_args()

    torch.manual_seed(19)
    wandb.init(project='berkeley-cs294-deep-reinforcement-learning', tags=['hw1', 'clone'], config=args)

    # Load dataset of expert policy rollouts
    dataset = ExpertPolicyRollouts(args.dataset)
    dataloader = data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True)

    # Create gym environment and load the clone policy
    env = gym.make(args.env_id)
    clone = clone_map[args.env_id](env.observation_space, env.action_space, load_weights=False)
    wandb.watch(clone, log='all')

    # Create model training infrastructure
    optimizer = optim.Adam(clone.parameters(), lr=args.lr)
    trainer = ignite.engine.create_supervised_trainer(clone, optimizer, nn.MSELoss())
    metrics = {'l1': ignite.metrics.Loss(nn.L1Loss()),
               'mse': ignite.metrics.Loss(nn.MSELoss()),
               'reward': EnvEpisodeReward(env, clone, 3)}
    evaluator = ignite.engine.create_supervised_evaluator(clone, metrics)

    # Build monitoring progress bar
    progress_bar = ignite.contrib.handlers.ProgressBar(persist=True)
    progress_bar.attach(trainer)

    @trainer.on(ignite.engine.Events.EPOCH_STARTED)
    def log_training_results(engine):
        metrics = evaluator.run(dataloader).metrics
        progress_bar.log_message(f"R = {metrics['reward']:.1f} | MSE = {metrics['mse']:.4f}")
        wandb.log(metrics)
        progress_bar.n = progress_bar.last_print_n = 0

    # Train the clone policy neural network
    trainer.run(dataloader, max_epochs=args.num_epochs)

    # Save the trained model
    clone.store()
