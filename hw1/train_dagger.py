#!/usr/bin/env python3

import bisect
import argparse
from pathlib import Path

import wandb
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import ignite.contrib.handlers

import gym
import roboschool

from run_clone import clone_map
from run_expert import expert_map, execute_rollout
from train_clone import EnvEpisodeReward


def relocate(clone_class):
    clone_class.weights_file = Path('daggers') / clone_class.weights_file.name
    return clone_class


dagger_map = {env: relocate(clone_class) for env, clone_class in clone_map.items()}


class AgregatedRollouts(data.Dataset):
    def __init__(self, observations=None, actions=None):
        super().__init__()
        self.observations = []
        self.actions = []
        self.cumulative_lengths = [0]
        if observations is not None and actions is not None:
            self.aggregate(observations, actions)

    def __getitem__(self, index):
        aggre_idx = bisect.bisect(self.cumulative_lengths, index) - 1
        local_idx = index - self.cumulative_lengths[aggre_idx]
        observation = self.observations[aggre_idx][local_idx]
        action = self.actions[aggre_idx][local_idx]
        return observation, action

    def __len__(self):
        return self.cumulative_lengths[-1]

    def aggregate(self, observations, actions):
        self.observations.append(torch.tensor(observations))
        self.actions.append(torch.tensor(actions))
        self.cumulative_lengths.append(self.cumulative_lengths[-1] + len(observations))


class DatasetLength(ignite.metrics.Metric):
    def __init__(self, dataset):
        super().__init__()
        self.dataset = dataset

    def reset(self):
        pass

    def update(self, output):
        pass

    def compute(self):
        return len(dataset)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Train a policy clone with expert dataset aggregation.')
    parser.add_argument('env_id', type=str, metavar='roboschool-env', choices=clone_map.keys(),
                        help='roboschool environment id')
    parser.add_argument('--num_aggregations', type=int, default=50, metavar='N',
                        help='number of expert dataset aggregation steps (default: 50)')
    parser.add_argument('--num_epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                        help='input batch size for training (default: 128)')
    parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
                        help='optimizer learning rate (default: 0.001)')
    args = parser.parse_args()

    torch.manual_seed(19)
    wandb.init(project='berkeley-cs294-deep-reinforcement-learning', tags=['hw1', 'dagger'], config=args)

    # Create gym environment and load the expert and dagger policies
    env = gym.make(args.env_id)
    expert = expert_map[args.env_id](env.observation_space, env.action_space)
    dagger = clone_map[args.env_id](env.observation_space, env.action_space, load_weights=False)
    wandb.watch(dagger, log='all')

    # Create aggregation dataset
    expert_observations, expert_actions, _ = execute_rollout(env, expert)
    dataset = AgregatedRollouts(expert_observations, expert_actions)

    # Create model training infrastructure
    optimizer = optim.Adam(dagger.parameters(), lr=args.lr)
    trainer = ignite.engine.create_supervised_trainer(dagger, optimizer, nn.L1Loss())
    metrics = {'l1': ignite.metrics.Loss(nn.L1Loss()),
               'mse': ignite.metrics.Loss(nn.MSELoss()),
               'reward': EnvEpisodeReward(env, dagger, 1),
               'dataset': DatasetLength(dataset)}
    evaluator = ignite.engine.create_supervised_evaluator(dagger, metrics)

    # Build monitoring progress bar
    progress_bar = ignite.contrib.handlers.ProgressBar(persist=True)
    progress_bar.attach(trainer)

    @trainer.on(ignite.engine.Events.EPOCH_STARTED)
    def log_training_results(engine):
        dataloader = data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True)
        metrics = evaluator.run(dataloader).metrics
        progress_bar.log_message(f"R = {metrics['reward']:.1f} | MSE = {metrics['mse']:.4f} | N = {metrics['dataset']}")
        wandb.log(metrics)
        progress_bar.n = progress_bar.last_print_n = 0

    # Dataset aggregation loop
    for aggregation in range(args.num_aggregations):
        dataloader = data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True)
        print(f"Aggregation [{aggregation}/{args.num_aggregations}]")

        # Train the dagger policy neural network
        trainer.run(dataloader, max_epochs=args.num_epochs)

        # Aggergate dataset with dagger policy rollout annotated by expert actions
        dagger_observations, _, _ = execute_rollout(env, dagger)
        expert_actions = expert.act(dagger_observations)
        dataset.aggregate(dagger_observations, expert_actions)

    # Save the trained model
    dagger.store()
