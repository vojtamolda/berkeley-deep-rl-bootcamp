from . import Policy
from pathlib import Path


class RoboschoolHumanoid(Policy):
    weights_file = Path(__file__).with_suffix('.pytorch')
    layer_sizes = (44, 256, 128, 17)
