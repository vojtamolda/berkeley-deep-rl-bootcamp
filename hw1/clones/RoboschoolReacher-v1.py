from . import Policy
from pathlib import Path


class RoboschoolReacher(Policy):
    weights_file = Path(__file__).with_suffix('.pytorch')
    layer_sizes = (9, 128, 64, 2)
