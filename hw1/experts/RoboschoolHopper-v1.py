from . import Policy
from pathlib import Path


class RoboschoolHopper(Policy):
    weights_file = Path(__file__).with_suffix('.weights')
    layer_sizes = (15, 128, 64, 3)
