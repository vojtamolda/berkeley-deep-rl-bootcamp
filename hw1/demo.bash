#!/usr/bin/env bash

set -eux

envs="RoboschoolAnt-v1 RoboschoolHalfCheetah-v1 RoboschoolHopper-v1 \
      RoboschoolHumanoid-v1 RoboschoolReacher-v1 RoboschoolWalker2d-v1"

for env in ${envs}; do
    # Showcase a rollout of expert policy
    python3 run_expert.py ${env} --render
done
