# CS294-112 HW 1: Imitation Learning

Dependencies:
 * Python **3.5**
 * Numpy version **1.14.5**
 * OpenAI Gym version **0.11.0**
 * OpenIA Roboschool version **1.0.34**

Once Python **3.5** is installed, you can install the remaining dependencies using `pip install -r requirements.txt`.

**Note**: Students enrolled in the course will receive an email with their MuJoCo activation key. Please do **not** share this key.

The only file that you need to look at is `run_expert.py`, which is code to load up an expert policy, run a specified number of roll-outs, and save out data.

In `run_expert.py`, the provided expert policies from roboschool model ZOO are:
* `RoboschoolAnt-v0`
* `RoboschoolHalfCheetah-v1`
* `RoboschoolHopper-v1`
* `RoboschoolHumanoid-v1`
* `RoboschoolReacher-v1`
* `RoboschoolWalker2d`
