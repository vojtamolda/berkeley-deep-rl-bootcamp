#!/usr/bin/env python3

import time
import pickle
import argparse
import numpy as np
from importlib import import_module

import gym
import roboschool


expert_map = {
    'RoboschoolAnt-v1': import_module('experts.RoboschoolAnt-v1').RoboschoolAnt,
    'RoboschoolHalfCheetah-v1': import_module('experts.RoboschoolHalfCheetah-v1').RoboschoolHalfCheetah,
    'RoboschoolHopper-v1': import_module('experts.RoboschoolHopper-v1').RoboschoolHopper,
    'RoboschoolHumanoid-v1': import_module('experts.RoboschoolHumanoid-v1').RoboschoolHumanoid,
    'RoboschoolReacher-v1': import_module('experts.RoboschoolReacher-v1').RoboschoolReacher,
    'RoboschoolWalker2d-v1': import_module('experts.RoboschoolWalker2d-v1').RoboschoolWalker2d,
}


def execute_rollout(env, policy, num_rollouts=1, render=False):
    observations, actions, rewards = [], [], []

    for rollout in range(num_rollouts):
        observation = env.reset()
        done, reward = False, 0
        while not done:
            observations.append(observation)
            action = policy.act(observation)
            actions.append(action)
            observation, rwrd, done, _ = env.step(action)
            reward += rwrd
            if render:
                env.render(mode='human')
                time.sleep(1 / 200)
        rewards.append(reward)

    return np.float32(observations), np.float32(actions), np.float32(rewards)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Load an expert policy and generate a dataset of rollouts')
    parser.add_argument('env_id', type=str, metavar='roboschool-env', choices=expert_map.keys(),
                        help='roboschool environment id')
    parser.add_argument('dataset', type=str, default=None, metavar='dataset', nargs='?',
                        help='pickle file for policy rollout dataset storage (optional)')
    parser.add_argument('--num_rollouts', type=int, default=1, metavar='N',
                        help='number of episodes to rollout (default: 1)')
    parser.add_argument('--render', action='store_true',
                        help='turn on visual rendering of the environment')
    args = parser.parse_args()

    # Create gym environment and load the expert policy
    env = gym.make(args.env_id)
    expert = expert_map[args.env_id](env.observation_space, env.action_space)

    # Execute policy rollouts in the environment
    observations, actions, rewards = execute_rollout(env, expert, args.num_rollouts, args.render)
    print(f'R = {np.mean(rewards):.1f} ± {np.std(rewards):.1f}')

    # Store dataset of observations, actions and rewards
    if args.dataset:
        dataset = {'observations': observations, 'actions': actions, 'rewards': rewards}
        pickle.dump(dataset, open(args.dataset, 'wb'))
