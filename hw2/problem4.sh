#!/usr/bin/env bash

rm -rf ./data/*
python3 train_pg_f18.py CartPole-v0 -n=100 -b=1000 -e=3 \
        --dont_normalize_advantages --exp_name=sb_no_rtg_dna
python3 train_pg_f18.py CartPole-v0 -n=100 -b=1000 -e=3 \
        --reward_to_go --dont_normalize_advantages --exp_name=sb_rtg_dna
python3 train_pg_f18.py CartPole-v0 -n=100 -b=1000 -e=3 \
        --reward_to_go --exp_name=sb_rtg_na
python3 plot.py ./data/ --value=AverageReturn

rm -rf ./data/*
python3 train_pg_f18.py CartPole-v0 -n=100 -b=5000 -e=3 \
        --dont_normalize_advantages --exp_name=lb_no_rtg_dna
python3 train_pg_f18.py CartPole-v0 -n=100 -b=5000 -e=3 \
        --reward_to_go --dont_normalize_advantages --exp_name=lb_rtg_dna
python3 train_pg_f18.py CartPole-v0 -n=100 -b=5000 -e=3 \
        --reward_to_go --exp_name=lb_rtg_na
python3 plot.py ./data/ --value=AverageReturn
