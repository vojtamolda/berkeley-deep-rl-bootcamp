#!/usr/bin/env bash

rm -rf ./data/*
python3 train_pg_f18.py RoboschoolInvertedPendulum-v1 -ep=1000 --discount=0.9 -n=300 -e=3 -l=2 -s=64 \
        -b=1500 -lr=0.01 --reward_to_go --exp_name opt_b_lr

python3 plot.py ./data/ --value=AverageReturn
